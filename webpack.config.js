const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require('copy-webpack-plugin');

var sass = require("sass");
var Fiber = require("fibers");
var $ = require("jquery");

sass.renderSync({
  file: "src/scss/app.scss",
  sourceMap: true,
});

module.exports = {
  entry: {
    main: path.resolve(__dirname, './src/js/app.js'),
  },
  output: {
    filename: 'js/[name].bundle.js',
    path: path.resolve(__dirname, 'deploy')
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }, ]
  },
  module: {
    rules: [{
        test: /\.s[ac]ss$/i,
        use: [
          // fallback to style-loader in development
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: ['url-loader?limit=8192&name=../images/[name].[ext]']
      },
      {
        test: /\.(woff|woff2)$/,
        use: ['url-loader?limit=8192&name=../fonts/[name].[ext]']
      }
    ],
  },
  plugins: [
    // new HtmlWebpackPlugin({
    //   title: "Webpack Output",
    // }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].css",
      chunkFilename: "[id].css",
    }),
    new CopyPlugin({
      patterns: [{
        from: 'src/images',
        to: 'images/'
      }, ],
    }),
    new CopyPlugin({
      patterns: [{
        from: 'src/fonts',
        to: 'fonts/'
      }, ],
    }),
    new CopyPlugin({
      patterns: [{
        from: 'src/index.html',
        to: './'
      }, ],
    }),

  ],
};
